# NeverThePreciseTimeServer

just a little NTP-Server keeping the time with a RTC-module and serving it to the poor souls in the network who do not have a RTC :-)

using a DS3231 based I2C RTC

Installs:

`sudo apt-get install i2c-tools` 
`sudo apt-get install python-smbus python3-smbus` 

*I2C-Bus scannen*

`i2cdetect -y 1`

-> 
0x68 = DS3231
0x57 = I2C EEPROM auf RTC-Modul

if there is an 'UU' in the table, some Kernel-driver already takes care of the RTC (e.g. *https://olfert.biz/tutorials/ein-rtc-modul-ds3231-am-raspberry-pi-betreiben/* )

*https://pimylifeup.com/raspberry-pi-rtc/*

`sudo nano /boot/config.txt'

add `dtoverlay=i2c-rtc,ds3231` at end of file

`sudo reboot` --> now `i2cdetect -y 1' should show the "UU"


Fake-HW-Clock loswerden:


`sudo systemctl stop fake-hwclock.service`
`sudo systemctl disable fake-hwclock.service`
`sudo apt-get remove fake-hwclock`
`sudo rm /etc/cron.hourly/fake-hwclock`
`sudo update-rc.d -f fake-hwclock remove`
`sudo rm /etc/init.d/fake-hwclock`
`sudo update-rc.d hwclock.sh enable`


`sudo nano /lib/udev/hwclock-set`
... den Block "if [ -e /run/systemd/system ] ; then exit 0 fi" auskommentieren

`sudo nano /etc/init.d/hwclock.sh' :

so machen:
`#if [ -d /run/udev ] || [ -d /dev/.udev ]; then`
`# return 0`
`#fi`

`sudo systemctl daemon-reload`

`sudo timedatectl` zeigt Infos

Uhrzeit/Datum aus der RTC zeigen:
`sudo hwclock -D -r`

Raspi hat im Moment aber diese Uhrzeit:
`date`

--> für richtige Uhrzeit am Raspi sorgen ( ans Internet, NTP, ... ), wenn's stimmt dann in die RTC schreiben:

`sudo hwclock -D -w`

testen: wartet, bis RTC-Clock tick macht, dann wird sofort "Raspi-Zeit" mit ausgeben

`sudo hwclock -r && date`

oder so:
`sudo hwclock --verbose --systohc --update-drift`

`hwclock --show --verbose` 



NTP wirklich ganz aus, nur bei Bedarf zum RTC-Stellen kurz wieder an:

https://www.raspberry-pi-geek.de/ausgaben/rpg/2015/03/echtzeituhr-modul-ds3231-sorgt-fuer-genaue-zeitangaben/2/


Als NTP-Server dienen:

`sudo apt-get install ntp ntpdate`

`sudo netstat -tulpena | grep -i 123` oder `sudo netstat -tulpen | grep -iE 'ntp|:123'` zum Client belauschen ?

Man muss alle Server auskommentieren und dann anstelle der Server
`server 127.127.1.0`
`fudge 127.127.1.0 stratum 0`
eingeben. Dann nimmt der Pi seine eigene Zeit als Referenzzeit. (Den Stratum kann man beliebig setzen. Da ich allerdings 
ihn als allgemeingültige Referenzzeit ansehe, habe ich ihn auf 0 gesetzt)


Liste aller NTP-Server zum Abfragen
`ntpq -p`

zum Spielen:
#http://support.ntp.org/bin/view/Servers/PublicTimeServer000388
server ntp.probe-networks.de prefer
#http://support.ntp.org/bin/view/Servers/PublicTimeServer001352
server time.hueske-edv.de prefer
#http://support.ntp.org/bin/view/Servers/PublicTimeServer000840
server ntp2.301-moved.de prefer
#http://support.ntp.org/bin/view/Servers/PublicTimeServer001363
server ntp.fanlin.de prefer



Material:

https://weberblog.net/ntp/

https://olfert.biz/tutorials/ein-rtc-modul-ds3231-am-raspberry-pi-betreiben/

https://www.raspberrypi.org/forums/viewtopic.php?t=218411
