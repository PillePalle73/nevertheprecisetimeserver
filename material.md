*Boot-Overlays mit Support für DS1307 oder DS3231 / FakeHW-Clock aus*

https://learn.adafruit.com/adding-a-real-time-clock-to-raspberry-pi/set-rtc-time

*Zeugs*

https://raspberrypi.stackexchange.com/questions/85579/setup-raspberry-pi3-as-ntp-server-not-using-external-ntp-server

https://forum-raspberrypi.de/forum/thread/40462-raspberry-als-timeserver-unter-stretch-wie-einrichten/

https://linuxconfig.org/how-to-setup-ntp-server-and-client-on-debian-9-stretch-linux

https://forum-raspberrypi.de/forum/thread/42679-rtc-ds3231-unter-raspbian-stretch-auf-einem-raspberry-pi-3b/

https://www.raspberrypi.org/forums/viewtopic.php?t=258559


*Spezialadresse 127.127.0.1*

https://forum-raspberrypi.de/forum/thread/12583-pi-als-ntp-server/


*LowLevel Python*

https://www.waveshare.com/wiki/Raspberry_Pi_Tutorial_Series:_RTC

https://github.com/switchdoclabs/RTC_SDL_DS3231


*reine HW*

http://www.intellamech.com/RaspberryPi-projects/rpi_RTCds3231

http://www.netzmafia.de/skripten/hardware/RasPi/Projekt-RTC/index.html

*minimal HW zum direkt-Aufstecken*

https://www.raspberry-pi-geek.de/ausgaben/rpg/2015/03/echtzeituhr-modul-ds3231-sorgt-fuer-genaue-zeitangaben/


*Prototyp mit Raspi 2 (mit PCF8564-RTC-Chip)*

http://anleitung.joy-it.net/?goods=exp-500

https://www.raspberrypi.org/forums/viewtopic.php?t=175202

https://www.youtube.com/watch?v=-0U-02Uhpag

